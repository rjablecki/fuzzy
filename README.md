## Fuzzy

Fuzzy ma być prostym narzędziem służącym do testowania aplikacji internetowych. Aplikacja jest skierowana do 
programistów i testerów. Podstawowym celem programu jest możliwość szybkiego i pełnego przetestowania 
endpointów aplikacji pod kątem:

* poprawnej implementacji walidacji
* właściwego przetwarzania danych wejściowych
* odporności aplikacji na różnego rodzaju ataki

## Jak uruchomić

Aby używać, po uruchomieniu wystarczy w przeglądarce internetowej otworzyć adres: __http://localhost:5000/__

## Funkcjonalności

* __modyfikacja parametrów__ - zmiana nazwy, wartości, przesyłanie tablic
* __fuzzing__ - możliwość wysłania jednocześnie kilkunastu asynchronicznych requestów 
podstawiając pod zmienną różne wartości, aby za jednym kliknięciem przetestować różne warunki
* __historia__ - wszystke requesty są zapisywane i można wrócić do poprzednich
* __mapa aplikacji__ - możliwość wgrania bazy linków wraz z parametrami, co znacznie przyspiesza testowanie aplikacji
* __cookie jar__ - możliwość wykonywywania testów po zalogowaniu
* __analiza odpowiedzi__ - czas, kod http, długość odpowiedzi, wyszukiwanie
* __podgląd outputu__ - można podglądać jak wygląda zrenderowana strona
* __generowanie raportu__ - po kliknięciu przycisku do schowka wrzucany jest komplet informacji, 
tak aby móc odtworzyć błąd w przyszłości

## Co w przyszłości ?

* użycie predefiniowanych wartości parametrów
* automatyczne pobieranie mapy endpointów
* automatyczne uruchomienie prostych testów całej aplikacji
* automatyczne wykrywanie podatności injection

## Development

Fuzzy jest jednoplikową aplikacją, a wszystkie pliki statyczne (JS, CSS, templatki) są wrzucane do binarki poprzez 
bibliotekę [gobuffalo/packr](https://github.com/gobuffalo/packr). Aby poprawnie zbudować aplikację należy: zainstalować 
packr w wersji v2 

~~~~
$ go get -u github.com/gobuffalo/packr/v2/...
~~~~

Samo budowanie aplikacji trzeba poprzedzić spakowaniem plików statycznych wywołując komendę `packr2`

~~~~
$ cd fuzzy
$ packr2
$ go build
~~~~