package app_log

import (
	"fmt"
	"time"
)

func Log(text string) {
	date := time.Now().Format("02.01.2006 15:04:05.99")
	fmt.Println(date+ " - "+ text)
}
