package main

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"
)

type RequestHistory []*FormData

var History RequestHistory

const HistoryRowHtml = `<a href=/?history=%d><button class="btn btn-primary" type="button">%s</button></a>`

func getTotalHistory(isHistory bool, historyId int) string {
	htmlString := ""
	historyLength := len(History)

	if !isHistory {
		historyId = historyLength - 1
	}

	if historyLength > 0 {
		if historyId > 0 {
			htmlString += fmt.Sprintf(
				HistoryRowHtml,
				historyId-1,
				"Prev",
			)
		}
		if historyId < historyLength-1 {
			htmlString += fmt.Sprintf(
				HistoryRowHtml,
				historyId+1,
				"Next",
			)
		}
	}

	return htmlString
}

func (h *RequestHistory) getQueryIntValue(r *http.Request, name string) (bool, int) {
	available := false
	var id int
	u, err := url.Parse(r.RequestURI)
	if err == nil {
		idString := u.Query().Get(name)
		if idString != "" {
			id, err = strconv.Atoi(idString)
			if err == nil && id >= 0 {
				available = true
			}
		}
	}

	return available, id
}
