package main

import (
	"fmt"
	"github.com/hoisie/mustache"
	"net/http"
	"net/url"
	"strings"
)

type FormData struct {
	Host      string
	Uri       string
	Cookies   string
	UserAgent string
	Params    string
	Set       string
	Method    string
}

const DefaultUserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3836.0 Safari/537.36"

const SetConst = "$SET"

func formHandler(w http.ResponseWriter, r *http.Request) {
	Crawler := &Crawler{}
	FormData := &FormData{}
	FormData.parsePostData(r)
	Crawler.FormData = FormData

	iframeOutputs = []string{}
	isHistory, historyIdInt := History.getQueryIntValue(r, "history")
	isSitemap, sitemapIdInt := History.getQueryIntValue(r, "sitemap")

	if isHistory && historyIdInt >= 0 && historyIdInt < len(History) {
		Crawler.FormData = History[historyIdInt]
	} else if isSitemap && sitemapIdInt >= 0 && sitemapIdInt < len(Sitemap) {
		Crawler.FormData = getSitemapFormData(sitemapIdInt)
		Crawler.Go()
	} else {
		Crawler.Go()
	}

	html := parseResponses(Crawler)

	Asset.ParseTemplateToOutput(w, "skeleton", struct {
		MainForm, HeaderInfo, ResponseInfo, History, JavaScript, Style, SitemapBlock string
	}{
		SitemapBlock: mustache.Render(Asset.GetTemplate("sitemap_block"), struct {
			SitemapHtml string
			IsSitemap   bool
		}{
			SitemapHtml: getSitemapMenu(Crawler.FormData.Uri),
			IsSitemap:   len(Sitemap) > 0,
		}),
		MainForm: mustache.Render(Asset.GetTemplate("form"), Crawler.FormData),
		HeaderInfo: fmt.Sprintf(
			`<b>%s</b> %d requests in %s `,
			Crawler.FormData.GetUrl(),
			len(Crawler.Responses),
			Crawler.TotalTime,
		),
		ResponseInfo: html,
		History:      getTotalHistory(isHistory, historyIdInt),
		JavaScript:   Asset.GetJs("app"),
		Style:        Asset.GetCss("style"),
	},
	)
}

func parseResponses(Crawler *Crawler) string {
	html := ""
	for responseId, response := range Crawler.Responses {
		if response != nil {
			html += mustache.Render(Asset.GetTemplate("one_response"), struct {
				HttpCode                  int
				Time, FormData            string
				ContentLength, ResponseId int
				ContentType, Body         string
			}{
				response.Response.StatusCode,
				response.Duration.String(),
				response.PostParams.Encode(),
				len(response.Content),
				responseId,
				response.Response.Header.Get("Content-Type"),
				response.Content,
			})
			iframeOutputs = append(iframeOutputs, response.Content)
		}
	}

	return html
}

func (F *FormData) GetUrl() string {
	return F.Host + F.Uri
}

func (F *FormData) parsePostData(r *http.Request) {
	host := r.FormValue("host")
	uri := r.FormValue("uri")
	userAgent := r.FormValue("user-agent")
	set := r.FormValue("set")
	params := r.FormValue("params")
	method := "POST"
	host = strings.TrimRight(host, "/")
	uri = prepareUri(uri)

	if userAgent == "" {
		userAgent = DefaultUserAgent
	}

	F.Host = host
	F.Uri = uri
	F.Cookies = r.FormValue("cookies")
	F.UserAgent = userAgent
	F.Params = params
	F.Set = set
	F.Method = method

}

func prepareUri(uri string) string {
	return "/" + strings.TrimLeft(uri, "/")
}

func parseSet(setContent string) []string {
	var res []string
	if setContent != "" {
		lines := strings.Split(setContent, "\n")
		for _, line := range lines {
			line = strings.TrimSpace(line)
			res = append(res, line)
		}
	}

	return res
}

func getReplacedParamName(postParams url.Values) string {
	for name, val := range postParams {
		if val[0] == SetConst {
			return name
		}
	}

	return ""
}

func parsePostParams(paramsContent string) url.Values {
	postParams := url.Values{}
	if paramsContent != "" {
		lines := strings.Split(paramsContent, "\n")
		for _, line := range lines {
			line = strings.TrimSpace(line)
			//@todo sprawdzić wyrażeniem regularnym
			if len(line) == 0 {
				continue
			}
			parts := strings.Split(line, "=")
			if len(parts) == 2 {
				postParams.Set(parts[0], parts[1])
			}
		}
	}

	return postParams
}
