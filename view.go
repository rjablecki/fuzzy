package main

import (
	"fmt"
	"github.com/gobuffalo/packr/v2"
	"github.com/hoisie/mustache"
	"net/http"
)

type PackrWrapper struct {
	View *packr.Box
	Js   *packr.Box
	Css  *packr.Box
}

var Asset *PackrWrapper

func init() {
	Asset = new(PackrWrapper)
	Asset.View = packr.New("viewBox", "./templates")
	Asset.Js = packr.New("jsBox", "./js")
	Asset.Css = packr.New("cssBox", "./css")
}

func (v *PackrWrapper) GetTemplate(name string) string {
	content, _ := v.View.FindString(name + ".mustache")

	return content
}

func (v *PackrWrapper) GetJs(name string) string {
	content, _ := v.Js.FindString(name + ".js")

	return content
}

func (v *PackrWrapper) GetCss(name string) string {
	content, _ := v.Css.FindString(name + ".css")

	return content
}

func (v *PackrWrapper) ParseTemplateToOutput(w http.ResponseWriter, templateName string, data interface{}) {
	fmt.Fprint(
		w,
		mustache.Render(
			Asset.GetTemplate("skeleton"),
			data,
		),
	)
}
