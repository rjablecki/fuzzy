package main

import (
	"fmt"
	"net/http"
)

var iframeOutputs []string

func iframeHandler(w http.ResponseWriter, r *http.Request) {
	htmlResponse := "invalid iframeId"
	isIframe, iframeId := History.getQueryIntValue(r, "id")
	if isIframe {
		if iframeId >= 0 && iframeId < len(iframeOutputs) {
			htmlResponse = iframeOutputs[iframeId]
		}
	}
	fmt.Fprint(w, htmlResponse)
}
