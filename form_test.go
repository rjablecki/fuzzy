package main

import (
	"net/url"
	"testing"
)

func TestParsePostParams(t *testing.T) {

	var tests = []struct {
		input    string
		expected string
	}{
		{"", url.Values{}.Encode()},
		{"id", url.Values{"id": []string{}}.Encode()},
		{"id=12", url.Values{"id": []string{"12"}}.Encode()},
		{`id=12
name=robert`, url.Values{"id": []string{"12"}, "name": []string{"robert"}}.Encode()},
	}

	for _, test := range tests {
		if output := parsePostParams(test.input).Encode(); output != test.expected {
			t.Error("Test Failed: {} inputted, {} expected, recieved: {}", test.input, test.expected, output)
		}

	}

}
