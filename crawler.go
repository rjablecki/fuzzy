package main

import (
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"
	"sync"
	"time"
)

type Crawler struct {
	Responses []*Response
	FormData  *FormData
	TotalTime time.Duration
}

type Response struct {
	Url        string
	Response   *http.Response
	Content    string
	Err        error
	Duration   time.Duration
	PostParams url.Values
}

var cookieJar *cookiejar.Jar

func init() {

	cookieJar, _ = cookiejar.New(nil)
}

func (C *Crawler) Go() {

	if C.FormData.Host == "" {
		return
	}
	var req []url.Values

	//@todo sprawdzać wyrażeniem regularnym
	useSet := strings.Contains(C.FormData.Params, SetConst)
	postParams := parsePostParams(C.FormData.Params)
	History = append(History, C.FormData)

	if useSet {
		dataSet := parseSet(C.FormData.Set)
		paramName := getReplacedParamName(postParams)
		for _, value := range dataSet {
			newPostParams := url.Values{}
			for name, v := range postParams {
				newPostParams.Set(name, v[0])
			}
			newPostParams.Set(paramName, value)
			req = append(req, newPostParams)
		}
	} else {
		req = append(req, postParams)
	}

	var wg sync.WaitGroup
	wg.Add(len(req))
	startTime := time.Now()
	for _, params := range req {
		go func(x url.Values) {
			defer wg.Done()
			response, _ := C.runOne(x)
			C.Responses = append(C.Responses, response)
		}(params)
	}
	wg.Wait()
	C.TotalTime = time.Now().Sub(startTime)
}

func (C *Crawler) passCookies(u *url.URL) {

	var beforeCookiesNames []string
	beforeCookies := cookieJar.Cookies(u)

	for _, beforeCookie := range beforeCookies {
		beforeCookiesNames = append(beforeCookiesNames, beforeCookie.Name)
	}

	var cookies []*http.Cookie
	formCookies := strings.Split(C.FormData.Cookies, ";")
	for _, formCookie := range formCookies {

		if len(strings.TrimSpace(formCookie)) == 0 {
			continue
		}
		oneCookie := strings.Split(formCookie, "=")
		if len(oneCookie) == 2 {
			cName := strings.TrimSpace(oneCookie[0])
			cVal := strings.TrimSpace(oneCookie[1])
			cVal = strings.TrimRight(cVal, ";")

			if len(cName) > 0 && len(cVal) > 0 {
				cookies = append(cookies, &http.Cookie{
					Name:  cName,
					Value: cVal,
				})
			}

		}
	}

BeforeLoop:
	for _, beforeCookieName := range beforeCookiesNames {
		for _, cookie := range cookies {
			if cookie.Name == beforeCookieName {
				continue BeforeLoop
			}
		}
		cookies = append(cookies, &http.Cookie{
			Name:    beforeCookieName,
			Value:   "-1",
			Expires: time.Unix(1414414788, 1414414788000),
		})

	}
	if len(cookies) > 0 {
		mainUrl, _ := url.Parse(u.Scheme + "://" + u.Host)
		cookieJar.SetCookies(mainUrl, cookies)
	}
}

func (C *Crawler) readCookies(url *url.URL) {
	C.FormData.Cookies = ""
	cks := cookieJar.Cookies(url)
	for _, a := range cks {
		C.FormData.Cookies += fmt.Sprintf("%s=%s; ", a.Name, a.Value)
	}
}

func (C *Crawler) runOne(postData url.Values) (*Response, error) {
	urlAddress := C.FormData.Host + C.FormData.Uri
	fmt.Printf("Fetching %s \n", urlAddress)

	transport := http.Transport{
		MaxIdleConns:    0,
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := http.Client{
		Transport: &transport,
		Jar:       cookieJar,
	}

	req, err := http.NewRequest(C.FormData.Method, urlAddress, strings.NewReader(postData.Encode()))

	if C.FormData.Method == "POST" && req != nil {
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	}

	if err != nil {
		dump("error NewRequest " + urlAddress)
		dump(err.Error())
		return nil, err
	}
	req.Header.Set("User-Agent", C.FormData.UserAgent)
	C.passCookies(req.URL)

	startTime := time.Now()
	resp, err := client.Do(req)
	endTime := time.Now()
	if err != nil {
		dump("error client.Do " + urlAddress)
		dump(err.Error())
		return nil, err
	}

	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		dump("error: ioutil.ReadAll")
		return nil, err
	}
	C.readCookies(req.URL)

	return &Response{
		Url:        urlAddress,
		Response:   resp,
		Content:    string(content),
		Duration:   endTime.Sub(startTime),
		PostParams: postData,
	}, nil
}
