var App = (function () {

    var $body = $('body');


    var copyToClipboard = function (text) {
        var $temp = $("<textarea>");
        $('body').append($temp);
        $temp.html(text).select();
        document.execCommand('copy');
        $temp.remove();
    };

    $body.find('.button-copy-request').click(function () {
        var $b = $(this);
        var $oneResponse = $b.closest('.one-response');
        var text = '';
        text += 'Request URL: ' + $body.find('#inputHost').val() + $body.find('#inputUri').val() + "\n";
        text += 'Request Method: POST' + "\n";
        text += 'Form Data: ' + $oneResponse.attr('data-form') + "\n";
        copyToClipboard(text);

        return false;
    });

    $body.on('input', '.find-input', function () {
        var $input = $(this);
        var $response = $input.closest('.one-response');
        var $counter = $response.find('.find-input-cnt');
        var $pre = $response.find('.response-body');
        var cnt = 0;
        $counter.text(cnt);
        $pre.unmark({
            done: function () {
                $pre.mark($input.val(), {
                    wildcards: 'enabled', each: function () {
                        cnt++;
                        $counter.text(cnt);
                    }
                });
            }
        });
    });


})();