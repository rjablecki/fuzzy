package main

import "fmt"

func dump(param interface{}) {
	fmt.Printf("%T", param)
	fmt.Printf(" (%+v)\n", param)
}
