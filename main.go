package main

import (
	applog "bitbucket.org/rjablecki/fuzzy/log"
	"net/http"
)

func main() {
	http.HandleFunc("/", formHandler)
	http.HandleFunc("/iframe", iframeHandler)
	http.HandleFunc("/sitemap", sitemapHandler)
	http.HandleFunc("/clear-sitemap", clearSitemapHandler)
	http.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {
	})

	log("Starting app on port 5000")

	err := http.ListenAndServe(":5000", nil)
	if err != nil {
		dump(err)
	}
}

func log(msg string) {
	applog.Log(msg)
}
