package main

import (
	"encoding/json"
	"fmt"
	"github.com/hoisie/mustache"
	"net/http"
	"regexp"
	"strings"
)

const ParamTypeString = "string"
const ParamTypeInt = "int"
const ParamTypeBool = "bool"
const ParamTypeArray = "array"

type Param struct {
	Name    string
	Type    string
	Value   string
	Default string
	Key     string
}

type Endpoint struct {
	Uri          string
	ResponseType string
	GetParams    []Param
	PostParams   []Param
	ValueParams  []Param
	Acm          string
}
type SitemapStruct []struct {
	Module string `json:"module"`
	Name   string `json:"name"`
	Action string `json:"action"`
	Link   string `json:"link"`
	Parent string `json:"parent,omitempty"`
	Acm    string `json:"acm,omitempty"`
	Params struct {
		Post []struct {
			Param   string      `json:"param"`
			Type    interface{} `json:"type"`
			Default interface{} `json:"default"`
			Key     interface{} `json:"key"`
		} `json:"post"`
		Value []struct {
			Param   string      `json:"param"`
			Type    interface{} `json:"type"`
			Default interface{} `json:"default"`
			Key     interface{} `json:"key"`
		} `json:"value"`
		Query []struct {
			Param   string      `json:"param"`
			Type    interface{} `json:"type"`
			Default interface{} `json:"default"`
			Key     interface{} `json:"key"`
		} `json:"query"`
	} `json:"params,omitempty"`
}

var Sitemap []Endpoint

func getSitemapMenu(uri string) string {
	var html string
	for i, s := range Sitemap {
		type Abc struct {
			Endpoint
			Id        int
			IsCurrent bool
		}
		item := Abc{s, i, uri == s.Uri}



		html += mustache.Render(Asset.GetTemplate("menu_item"), item)
	}

	return html
}

func getSitemapFormData(sitemapIdInt int) *FormData {
	s := Sitemap[sitemapIdInt]
	var fd *FormData

	if len(History) > 0 {
		fd = History[len(History)-1]
	} else {
		fd = new(FormData)
	}

	fd.Uri = s.Uri

	params := ""
	for _, p := range s.PostParams {
		var val string

		name := p.Name
		if p.Key != "" {
			name = p.Key
		}

		if p.Default != "" {
			val = p.Default
		} else if p.Type == ParamTypeInt {
			val = "1234"
		} else if p.Type == ParamTypeBool {
			val = "true"
		} else if p.Type == ParamTypeArray {
			val = "test"
			name += "[]"
		} else {
			val = "test"
		}

		params += fmt.Sprintf("%s=%s\n", name, val)
	}

	fd.Params = params

	return fd
}

func sitemapHandler(w http.ResponseWriter, r *http.Request) {
	s := r.FormValue("sitemap")
	SendSitemap := SitemapStruct{}
	isError := json.Unmarshal([]byte(s), &SendSitemap)

	if isError != nil && !checkIsText(s) {
		fmt.Fprint(w, isError.Error())
		return
	}
	for _, one := range SendSitemap {
		endpoint := Endpoint{}
		endpoint.Uri = one.Link
		endpoint.Acm = one.Acm
		endpoint.ResponseType = one.Parent
		for _, p := range one.Params.Post {
			if p.Param == "-" {
				continue
			}
			pp := Param{}
			pp.Name = p.Param
			pp.Type = fmt.Sprintf("%s", p.Type)
			pp.Default = fmt.Sprintf("%s", p.Default)
			pp.Key = fmt.Sprintf("%s", p.Key)
			endpoint.PostParams = append(endpoint.PostParams, pp)
		}
		Sitemap = append(Sitemap, endpoint)
	}
	http.Redirect(w, r, "/", 302)
}

func clearSitemapHandler(w http.ResponseWriter, r *http.Request) {
	Sitemap = []Endpoint{}

	http.Redirect(w, r, "/", 302)
}

func checkIsText(text string) bool {
	links := []string{}
	lines := strings.Split(text, "\n")
	for _, line := range lines {
		line := strings.TrimSpace(line)
		if len(line) == 0 {
			continue
		}
		r, _ := regexp.Compile(`^/[^\s]*$`)
		if r.MatchString(line) {
			links = append(links, line)
		}
	}

	if len(links) == 0 {
		return false
	}

	for _, link := range links {
		endpoint := Endpoint{}
		endpoint.Uri = link
		Sitemap = append(Sitemap, endpoint)
	}

	return true
}
